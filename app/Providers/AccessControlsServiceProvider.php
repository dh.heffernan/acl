<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;


class AccessControlsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap Access Controls Blade Directives.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('ifrole', function ($expression) {
            return "<?php if (User::has({$expression}) === true): ?>";
        });
        Blade::directive('ifrolenot', function ($expression) {
            return "<?php if (User::has({$expression}) === false): ?>";
        });
        Blade::directive('elseifrole', function ($expression) {
            return "<?php elseif (User::has({$expression}) === true): ?>";
        });
        Blade::directive('ifcan', function ($expression) {
            return "<?php if (User::canAccess({$expression}) === true): ?>";
        });
        Blade::directive('elseifcan', function ($expression) {
            return "<?php elseif (User::canAccess({$expression}) === true): ?>";
        });        
    }

    public function register()
    {
        \App::bind('User', function() {
            return new \App\AccessControls\AccessControls();
        });
    }
}