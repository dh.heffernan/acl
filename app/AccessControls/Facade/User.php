<?php

namespace App\AccessControls\Facade;  
 
use Illuminate\Support\Facades\Facade;  
 
class User extends Facade 
{
	protected static function getFacadeAccessor()
	{
		return 'User';
	}
}