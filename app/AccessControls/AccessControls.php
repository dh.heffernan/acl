<?php

namespace App\AccessControls;

class AccessControls
{
	public function has($roles)
    {
        return \Auth::user()->has($roles);
    }

    public function canAccess($permissions)
    {
        return \Auth::user()->canAccess($permissions);
    }
}