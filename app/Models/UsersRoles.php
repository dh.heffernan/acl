<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Grant;

class UsersRoles extends Model
{
    use SoftDeletes, Grant;
    
    protected $table = 'users_roles';
    
    protected $fillable = [
        'user_id',
        'role_id'
    ];

    protected $dates = ['deleted_at'];
}