<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Grant;

class Roles extends Model
{
    use SoftDeletes, Grant;
    
    protected $table = 'roles';
    
    protected $fillable = [
        'name',
        'slug'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permissions', 'roles_permissions', 'role_id', 'permission_id');
    }
}