<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Grant;

class Permissions extends Model
{
    use SoftDeletes, Grant;
    
    protected $table = 'permissions';
    
    protected $fillable = [
        'name',
        'slug'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Roles', 'roles_permissions', 'role_id', 'permission_id');
    }
}