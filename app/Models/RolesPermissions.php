<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Grant;

class RolesPermissions extends Model
{
    use SoftDeletes, Grant;
    
    protected $table = 'roles_permissions';
    
    protected $fillable = [
        'role_id',
        'permission_id'
    ];

    protected $dates = ['deleted_at'];
}