<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permissions;
use App\Models\Roles;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('dashboard.create-role', ['data' => Permissions::get()]);
    }

    public function store(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:roles',
        ];
        $validation = \Validator::make($input, $rules);
        if ($validation->fails()) {
            return redirect('/roles/create')->withErrors($validation)->withInput($input);
        } else {
            $permissions = null;
            if (isset($input['permissions'])) {
                $permissions = $input['permissions'];
            }
            unset($input['permissions']);
            $role = Roles::create($input);
            if (is_array($permissions) && !empty($permissions)) {
                foreach ($permissions as $permission) {
                    $role->permissions()->attach($permission);
                }
            } else {
                if ($permissions != null) {
                    $role->permissions()->attach($permissions);    
                }                
            }
            return redirect('/users');   
        }
    }

    public function edit($id)
    {
        $data = new \stdClass();
        $data->role = Roles::with('permissions')->find($id);
        $permissions = [];
        foreach ($data->role->permissions as $permission) {
            $permissions[] = $permission->id;
        }
        $data->role_permissions = $permissions;
        $data->permission_list = $this->list();
        return view('dashboard.edit-role', ['data' => $data]);
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'id'   => 'required',
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
        ];
        $validation = \Validator::make($input, $rules);
        if ($validation->fails()) {
            return redirect('/roles/edit/' . $request->input('id'))->withErrors($validation)->withInput($input);
        } else {
            $permissions = null;
            if (isset($input['permissions'])) {
                $permissions = $input['permissions'];
            }
            unset($input['permissions']);
            $role = Roles::with('permissions')->find($input['id']);
            foreach ($input as $key => $value) {
                $role->$key = $value;                                
            }
            $role->save();
            $role->permissions()->sync($permissions);
            return redirect('/users');
        }
    }

    public function delete()
    {
        $input = $request->except('_token');
        $rules = [
            'id' => 'required'
        ];
        $validation = \Validator::make($input, $rules);
        if ($validation->passes()) {
            $role = Roles::with('permissions')->find($input['id']);
            $permissions = [];
            foreach ($role->permissions as $permission) {
                $permissions[] = $permission->id;
            }
            $role->permissions()->detach($permissions);
            $role->delete();
        }
    }

    public static function list()
    {
        $rows = Permissions::select('id', 'name')->get();
        $list = [];
        foreach ($rows as $row) {
            $list[$row->id] = $row->name;
        }
        return $list;
    }
}
