<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permissions;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('dashboard.pages.user-management.create-permission');
    }

    public function store(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255|unique:permissions',
        ];
        $validation = \Validator::make($input, $rules);
        if ($validation->fails()) {
            return redirect('/permissions/create')->withErrors($validation)->withInput($input);
        } else {
            Permissions::create($input);
            return redirect('/users'); 
        }
    }

    public function edit($id)
    {
        return view('dashboard.pages.user-management.edit-permission', ['data' => Permissions::find($id)]);
    }

    public function update(Request $request)
    {
        $input = $request->except('_token');
        $rules = [
            'id'   => 'required',
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
        ];
        $validation = \Validator::make($input, $rules);
        if ($validation->fails()) {
            return redirect('/permissions/edit/' . $request->input('id'))->withErrors($validation)->withInput($input);
        } else {
            $permission = Permissions::find($input['id']);
            foreach ($input as $key => $value) {
                $permission->$key = $value;                
            }
            $permission->save();
            return redirect('/users');
        }
    }

    public function delete()
    {
        $input = $request->except('_token');
        $rules = [
            'id' => 'required'
        ];
        $validation = \Validator::make($input, $rules);
        if ($validation->passes()) {
            $permission = Permissions::with('roles')->find($input['id']);
            $roles = [];
            foreach ($permission->roles as $role) {
                array_push($roles, $role->id);
            }
            $permission->roles()->detach($roles);
            $permission->delete();
        }
    }
}
