<?php

namespace App\Http\Middleware;

use Closure;
use \App\AccessControls\Facade\User;

class Permissions
{
    public function handle($request, Closure $next, $permisssions)
    {
        if (User::canAccess($permisssions) === true) {
            return $next($request);
        } else {
            if ($request->isJson()) {
                return response()->json(['error' => 'Access denied'], 403);
            } else {
                abort(403);
            }
        }
    }
}