<?php

namespace App\Http\Middleware;

use Closure;
use \App\AccessControls\Facade\User;

class Roles
{
    public function handle($request, Closure $next, $roles)
    {
        if (User::has($roles) === true) {
            return $next($request);
        } else {
            if ($request->isJson()) {
                return response()->json(['error' => 'Access denied for role'], 403);
            } else {
                abort(403);
            }
        }
    }
}