<?php
namespace App\Traits;

use App\Models\Roles;
use App\Models\Permissions;

trait Grant
{
    public function has($slugs)
    {
        $user = \Auth::user();
        if (!is_array($slugs)) {
            $slugs = explode('|', $slugs);
        } else {
            $slugs = array_values($slugs);
        }
        foreach ($slugs as $key => $value) {
            $slugs[$key] = $value;
        }
        foreach ($user->roles as $role) {
            if (in_array($role->slug, $slugs) === true) {
                return true;
            }                
        }
        return false;
    }        

    public function canAccess($slugs)
    {
        $user = \Auth::user();
        if (!is_array($slugs)) {
            $slugs = explode('|', $slugs);
        } else {
            $slugs = array_values($slugs);
        }
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $permission) {
                if (in_array($permission->slug, $slugs) === true) {
                    return true;
                }
            }
        }
        return false;
    }
}